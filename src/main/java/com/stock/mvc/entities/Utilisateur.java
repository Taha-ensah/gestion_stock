package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "utilisateur")
public class Utilisateur implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idUtilisiteur;
	
	private String nom;
	
	private String prenom;
	
	private String mail;
	
	private String motDePass;
	
	private String photo;

	public Long getIdUtilisiteur() {
		return idUtilisiteur;
	}

	public void setIdUtilisiteur(Long idUtilisiteur) {
		this.idUtilisiteur = idUtilisiteur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMotDePass() {
		return motDePass;
	}

	public void setMotDePass(String motDePass) {
		this.motDePass = motDePass;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}	
	
}
